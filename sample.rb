#!/usr/bin/env ruby
#--
# Copyright (c) 2016 Mobily.ws
# Code by Lucas Thompson
#
# Script showing how to import parts of library and use them.
#
# For more examples, read the manual (README.md)
#++

require_relative 'lib/mobily_sms'

if MobilySMS.can_send?
  puts 'Service is available!'
else
  puts 'Service is not available!'
end