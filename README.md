# mobily-ruby

## Getting Started
There is a small script, `sample.rb`, which demonstrates how to import parts of the library and use them,
run it from the terminal with:

```bash
ruby sample.rb
```

## Examples
*All examples assume you have a mobily.ws account with an available balance*

**_Managing your mobily.ws account_**

Request to change account password:

```ruby
require_relative 'lib/mobily_api_auth'
require_relative 'lib/mobily_account'
account = MobilyAccount.new(MobilyApiAuth.new('966555555555', 'demo'))
account.change_password('TrustNo1')
```

Get current password sent to email or phone:

```ruby
account.forgot_password  # send to email registered on account
account.forgot_password(false)  # send to phone registered on account
```

Check the available balance on the account:

```ruby
balance = account.check_balance
puts '%s credits available, total %s' % [balance['current'], balance['total']]
```


**_Configuring Senders_**

Check the activation status of all previously requested senders:

```ruby
require_relative 'lib/mobily_api_auth'
require_relative 'lib/mobily_sender'

sender = MobilySender.new(MobilyApiAuth.new('966555555555', 'demo'))
senders_by_status = sender.get_activation_status_for_all_senders
print 'Active Senders: ', senders_by_status['active'].each { |x| x }
print 'Pending Senders: ', senders_by_status['pending'].each { |x| x }
print 'Inactive Senders: ', senders_by_status['notActive'].each { |x| x }
```

Request to add a new sender word name:

```ruby
sender.request_alphabetical_license('NEW SMS')
```

Two step process for activating a mobile number as a sender name:

```ruby
sender_id = sender.request_mobile_number_license('966444444444')
#  the above call returns an id if successful, and a code is sent via SMS to the number
sender.activate_mobile_number_license(sender_id, 'CODE_FROM_SMS')

#  check it worked
if sender.is_mobile_number_license_active?(sender_id)
  puts 'Activated!'
end
```


**_Sending SMS messages_**

Check the Mobily.ws SMS sending service is available:

```ruby
require_relative 'lib/mobily_sms'
if MobilySMS.can_send?
  puts 'Service is available!'
end
```

Send SMS, immediately, saying 'Hello, World' to 966444444444, from 'RUBY':

```ruby
require_relative 'lib/mobily_api_auth'
require_relative 'lib/mobily_sms'
sms = MobilySMS.new(MobilyApiAuth.new('966555555555', 'demo'))
sms.add_number('966444444444')
sms.sender = 'RUBY'
sms.msg = 'Hello, World!'
sms.send
```

As above, but using constructor, and sending to multiple numbers:

```ruby
auth = MobilyApiAuth.new('966555555555', 'demo')
sms = MobilySMS.new(auth, ['96202258669', '967965811686'], 'RUBY', 'Hello, World!')
sms.send
```

As above, but schedule to send on 25th December 2020 at midday:

```ruby
auth = MobilyApiAuth.new('966555555555', 'demo')
sms = MobilySMS.new(auth, ['96202258669', '967965811686'], 'RUBY', 'Hello, World!')
sms.schedule_to_send_on(25, 12, 2020, 12, 0, 0)
sms.delete_key = '666'
sms.send
```

Delete the above scheduled SMS before it sends:

```ruby
sms.delete
```

Send a bulk SMS to multiple people, letting them know about their subscription, with personalised messages just for them:

```ruby
require_relative 'lib/mobily_api_auth'
require_relative 'lib/mobily_formatted_sms'

auth = MobilyApiAuth.new('966555555555', 'demo')
msg = 'Hi (1), your subscription will end on (2).'
sms = MobilyFormattedSMS.new(auth, ['966505555555', '966504444444'], 'NEW SMS', msg)
sms.add_variable_for_number('966505555555', '(1)', 'Ahmad')
sms.add_variable_for_number('966505555555', '(2)', '31/12/2013')
sms.add_variable_for_number('966504444444', '(1)', 'Mohamed')
sms.add_variable_for_number('966504444444', '(2)', '01/11/2013')
sms.send
```


**_Handling errors_**

When a request has been unsuccessful, whether due to a known error (insufficient balance), or otherwise, an MobilyApiError is raised.

This error contains a message in English and Arabic.

```ruby
require_relative 'lib/mobily_api_auth'
require_relative 'lib/mobily_api_error'
require_relative 'lib/mobily_account'

account = MobilyAccount.new(MobilyApiAuth.new('DOESNT_EXIST', 'demo'))
begin
  response = account.check_balance
rescue MobilyApiError => error
  puts error.msg_english, error.msg_arabic
end
```


## Tests
Tests for the core logic behind the utilities can be run from the terminal with:
```bash
     rake test
```